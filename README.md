# Welcome package (Computing) on first day and second day = Dense/compressed Linux, Grid and ATLAS offline Software Tutorial

## Interactive exercises - In principle, 45 mins explanation, 10 mins rest and 45 mins exercise

## Environments
       # Connect
       ssh -t -C -p 24 gen@login.ph2.physik.uni-goettingen.de ssh -t -C exercise0@pcatlas46
       # Attach a screen session
       screen -x
       # or open a new screen session
       screen

* Students must use **SCREEN** sessions. Make several groups by 2-3 students. Share a **SCREEN* in a group. For example, one student does the exercise, the others will watch the terminal, read, understand the materials and give advices.

 --> For Tutor: a tutor can open all students terminal **SCREENs** by a **TMUX** screen in a terminal. See and share them by a beamer screen.


## A Grid certificate during these exercises. A tutor provides a temporary proxy certificate (e.g. =/tmp/x509up_u1000)
     # 0. Check a proxy certificate
     ls -al /tmp/x509up_u1000

     # 1. Set proxy certificate
     # 2. You can copy and change the permission
     export X509_USER_PROXY=/tmp/temp_cert.$USER
     cp -v /tmp/x509up_u1000 $X509_USER_PROXY

     # 4. Permission is (User, Group, Others) = (ReadWrite, -, -)
     chmod -v 600 $X509_USER_PROXY



## RSA and Grid Keys
* How to generate private/public RSA keys (for CERN GitLab)

      ssh-keygen -t rsa -b 2000

      * You can find ~/.ssh/id_rsa.pub (RSA public key) 

      * Open https://gitlab.cern.ch/profile/keys --> put your public key (=~/.ssh/id_rsa.pub) onto GitLab at CERN
      
      * When one creates own project, one can find *SSH* protocol. Copy and paste it to your git project config file (= .git/config)


* Request your Grid certificate (for your own Grid jobs in future)
https://gridka-ca.kit.edu/

  Follow the procedure: http://wiki.ph2.physik.uni-goettingen.de/wiki/index.php/How_to_join_the_ATLAS_VO


## Targets and ToDo for first day
* Tutorials of Linux, Emacs (+Spell checker), Bash (Scripting) **SCREEN** and some concepts of Version Control Systems (VCS)
* Grid tutorial in ATLAS-D (Concept of Grid, Certificate, PanDA, Rucio and CVMFS)
* Finally, put a student's exercise note onto his/her Git/VCS repository (*GitHub* https://github.com/).

## Targets and ToDo for second day
* A bit to touch the GDB exersice (http://wiki.ph2.physik.uni-goettingen.de/wiki/index.php/Welcome_Package#debugger.2C_profiler)
* ATLAS Event Data Model
* Athena JobOption file
* Derivation Framework
* Version Control System (VCS) - *CERN GitLab* https://gitlab.cern.ch
* **Write student's summary by [Markdown](https://en.wikipedia.org/wiki/Markdown) (like this text) on CERN GitLab** --> At 16:00, explanations, questions and feedbacks

----------------------------------------------------------
## Main links for first day

* ATLAS-D Computing Tutorial and exercise

https://github.com/GenKawamura/ATLAS-D_2017_Computing_Tutorial

* Welcome package Linux, Emacs

http://wiki.ph2.physik.uni-goettingen.de/wiki/index.php/Welcome_Package#Computing

* Welcome Package Cheet Note

http://wiki.ph2.physik.uni-goettingen.de/wiki/upload/5/5b/Cheat_note.txt

----------------------------------------------------------
## Main links for second day
* ATLAS Offline Software Computing Tutorial

https://indico.cern.ch/event/684668/timetable/

* Welcome Package Cheet Note

http://wiki.ph2.physik.uni-goettingen.de/wiki/upload/5/5b/Cheat_note.txt

----------------------------------------------------------
# Main links from ATLAS Offline Software Tutorial (22-26.Jan.2018)
* https://indico.cern.ch/event/684668/timetable/

## General (22.01.2018)
* ATLAS DATA - ATLAS Computing and Data Preparation

https://indico.cern.ch/event/684668/contributions/2863291/attachments/1586619/2509581/20180122_dp_induction.pdf

* How to do a PhD (or otherwise be new to ATLAS) (22.01.2018)

https://indico.cern.ch/event/684668/contributions/2807366/attachments/1586891/2509461/HowToDoAPhD_ECSB_VMMCAIRO_22Jan2018.pdf

## ATLAS EDM (22.01.2018)
* How to do ATLAS Analysis

https://indico.cern.ch/event/684668/contributions/2807365/attachments/1586896/2509469/20180122_HowToDoAnAnalysis.pdf

## ATLAS EDM (23.01.2018)
* Welcome to the tutorial and basics of ATLAS Software

https://indico.cern.ch/event/684668/contributions/2807398/attachments/1587197/2510119/Intro_AnalyModel.pdf

* Hands-on Athena (23.01.2018)

https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialSoftwareBasics

* An Introduction to the ATLAS Event Data Model

https://indico.cern.ch/event/684668/contributions/2807395/attachments/1587494/2510527/LHeelan_EDM.pdf

* The ATLAS Data Model
extra_slides/TheATLASDataModel.pdf

* ATLAS EDM Hands-on

https://indico.cern.ch/event/684668/contributions/2807355/attachments/1586345/2508380/go

## ATLAS EDM Analysis (24.01.2018)
* Introduction to xAOD Analysis in AthAnalysis

https://indico.cern.ch/event/684668/contributions/2807422/attachments/1588444/2512515/AthAnalysisTutorialJan2018.pdf

* xAODAnalysis Hands-on (24.01.2018)

https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake

* More Athena examples
https://twiki.cern.ch/twiki/bin/view/PanDA/PandaAthena

## Derivation framework (26.01.2018)
* Derivation Framework

https://indico.cern.ch/event/684668/contributions/2807383/attachments/1590074/2515972/DF_Tutorial_2018.01.26.pdf

* Hands-on Derivation Framework

https://indico.cern.ch/event/684668/contributions/2807384/attachments/1586351/2508387/go


## Students did Grid tutorial on the first day, but to look at them again
* Overview of Rucio
https://indico.cern.ch/event/684668/contributions/2807433/attachments/1589336/2514424/go

* Grid tools: Panda
https://indico.cern.ch/event/684668/contributions/2807434/attachments/1589392/2514528/Panda_Intro_Nurcan_Jan2017_new.pdf

* Hands-on Panda and Rucio

https://indico.cern.ch/event/684668/contributions/2807435/attachments/1586349/2508384/go

https://indico.cern.ch/event/684668/contributions/2807435/attachments/1586349/2508385/go


## CERN Gitlab (24, 25.01.2018)
* Git for Analysis

https://indico.cern.ch/event/684668/contributions/2807391/attachments/1589641/2515024/Git_tutorial.pdf

https://indico.cern.ch/event/684668/contributions/2807390/attachments/1589757/2515243/GitForAnalysis25012018.pdf

* Lerning Git - Details

https://twiki.cern.ch/twiki/bin/view/Main/LearningGit

* Git for Analysis

https://indico.cern.ch/event/684668/contributions/2807389/attachments/1586350/2508386/go

* Using GitLab for Analysis Code Management

https://indico.cern.ch/event/684668/contributions/2850676/attachments/1589546/2514835/ContinuousIntegration.pdf

* Docker Analysis Release Containers

https://indico.cern.ch/event/684668/contributions/2850677/attachments/1590190/2516207/DockerATLASTutorial.pdf
